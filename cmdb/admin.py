# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from models import Host, HostGroup, IpSource, Idc, InterFace,Project,Manufactory,env


class HostAdmin(admin.ModelAdmin):
    list_display = [
        'hostname',
        'ip',
        'group',
        'vendor',
        'os',
        'cpu_model',
        'cpu_num',
        'sn',
        #'identity',
        ]

class ManufactoryAdmin(admin.ModelAdmin):
    list_display = ['vendor_name',
                    'asset_type',
                    ]


class HostGroupAdmin(admin.ModelAdmin):
    list_display = ['name',]


class IpAdmin(admin.ModelAdmin):
    list_display = ['vlan_id','env','subnet']


class IdcAdmin(admin.ModelAdmin):
    list_display = ['name',
                    'address',
                    ]

class ProjectAdmin(admin.ModelAdmin):
    ordering = ('-parent',)
    list_filter = ('name',)
    list_display = ['name', 'parent', 'show', 'url', 'priority', 'permission_id']
    fields = ['name', 'parent', 'show', 'url', 'priority', 'permission_id']

class InterFaceAdmin(admin.ModelAdmin):
    list_display = ['name',]

admin.site.register(Host, HostAdmin)
admin.site.register(IpSource, IpAdmin)
admin.site.register(Idc, IdcAdmin)
admin.site.register(InterFace, InterFaceAdmin)
admin.site.register(HostGroup, HostGroupAdmin)
admin.site.register(Project,ProjectAdmin)
admin.site.register(Manufactory,ManufactoryAdmin)
admin.site.register(env)