# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpRequest,HttpResponse
from django.shortcuts import render
from django.core.paginator import Paginator
from saltcore.salt_core import *
from minions.models import Minions_status
from returner.models import Salt_grains
from django.views.generic import ListView,DetailView
from django.views.generic.detail import SingleObjectTemplateResponseMixin,BaseDetailView
from cmdb.models import Project
from website.JSONResponseMixin import JSONResponseMixin
import logging
import json


class ProjectListView(ListView):
    template_name = "publisher/Project.list.html"
    context_object_name = 'projectlist'
    model = Project

class ProjectDetailView(JSONResponseMixin, BaseDetailView):
    def get(self, request, *args, **kwargs):
        id = request.POST.get('id')
        data = Project.objects.get(id=id)
        self.render_to_response(data)

    def render_to_response(self, context):
            return JSONResponseMixin.render_to_response(self, context)