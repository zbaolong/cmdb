# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.

class Project(models.Model):
    name = models.CharField(u"名称", max_length=30, unique=True)
    owner = models.ManyToManyField(settings.AUTH_USER_MODEL, default = "null",)
    parent = models.ForeignKey('self',
                               verbose_name = u'父级菜单',
                               null = True,
                               blank = True,
                               default = '0',
                               help_text = u'如果添加的是子菜单，请选择父菜单'
                               )
    show = models.BooleanField(verbose_name=u'是否显示',
                               default=True,
                               help_text=u'菜单是否显示，默认添加不显示')

    url = models.CharField(max_length=300,
                           verbose_name=u'菜单url地址',
                           null=True,
                           blank=True,
                           default='javascript:void(0)',
                           help_text=u'是否给菜单设置一个url地址')

    priority = models.IntegerField(verbose_name=u'显示优先级',
                                   null=True,
                                   blank=True,
                                   default=-1,
                                   help_text=u'菜单的显示顺序，优先级越大显示越靠前')

    permission_id = models.IntegerField(verbose_name=u'权限编号',
                                        help_text=u'给菜单设置一个编号，用于权限控制',
                                        error_messages={'field-permission_id': u'只能输入数字'})

    def __str__(self):
        return "{parent}{name}".format(name=self.name, parent="%s-->" % self.parent.name if self.parent else '')

    class Meta:
        verbose_name = u"项目配置"
        verbose_name_plural = u"项目配置"
        ordering = ["-priority", "id"]

    def __unicode__(self):
        return self.name